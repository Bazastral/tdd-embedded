
#include <stdint.h>

#include "hw_api.h"
#include "heartbeat.h"



#ifdef DEBUG
#include "stdio.h"
#define DEBUG_LOG(x ...) printf(x)
#else
#define DEBUG_LOG(x ...)
#endif


/**
 * Assumptions
 * 
 * Period means two changes (two edges)
 * Proper flow:
 * -  9 hi   9 low  9 hi   9 low  9 hi   9 low   TOTAL  54
 * -  9 low  9 hi   9 low  9 hi   9 low  9 h     TOTAL  54
 * - 10 hi  10 low 10 hi  10 low 10 hi  10 lo    TOTAL  60
 * - 10 low 10 hi  10 low 10 hi  10 low 10 h     TOTAL  60
 * - 11 hi  11 low 11 hi  11 low 11 hi  11 lo    TOTAL  66
 * - 11 low 11 hi  11 low 11 hi  11 low 11 h     TOTAL  66
 */

// under LastIdx is the newest one
pinlevel_t LastStates[HEARTBEAT_TIMESPAN_MAX];
uint8_t LastIdx = 0u;

void heartbeat_thread(void)
{
    if (LastIdx + 1u >= HEARTBEAT_TIMESPAN_MAX)
    {
        LastIdx = 0u;
    }
    else
    {
        LastIdx++;
    }
    LastStates[LastIdx] = heartbeat_pin_get();
}

static pinlevel_t GetSampleAt(uint8_t index)
{
    if(index > LastIdx)
    {
        LastIdx += HEARTBEAT_TIMESPAN_MAX;
    }
    return LastStates[LastIdx - index];
}

heartbeat_status_t heartbeat_get_status(void)
{
    DEBUG_LOG("\n GET STATUS ------------------\n");
    // firstly check if there are enough samples
    for(uint8_t i = 0u; i < HEARTBEAT_TIMESPAN_MIN; i++)
    {
        if(GetSampleAt(i) == PINSTATE_HIZ)
        {
            DEBUG_LOG("too small amount of data\n");
            return STATUS_UNKNOWN;
        }
    }

    pinlevel_t PrevState = GetSampleAt(0);
    uint8_t PrevStateChange = 0u;
    uint8_t HalfPeriodsOK = 0u;

    DEBUG_LOG("LastStates:  %u %u %u %u %u\n", 
    GetSampleAt(0),
    GetSampleAt(1),
    GetSampleAt(2),
    GetSampleAt(3),
    GetSampleAt(4)
     );

    for(uint8_t i = 0u; i < sizeof(LastStates)/sizeof(LastStates[0u]); i++)
    {
        if (i == sizeof(LastStates) / sizeof(LastStates[0]) - 1u)
        {
            DEBUG_LOG("Last iteration\n");
        }
        // TODO think about optimization,
        // maybe its better to check here longest period first, not change in state
        if (GetSampleAt(i) != PrevState)
        {
            DEBUG_LOG("Change from %u to %u, at %u\n", PrevState, GetSampleAt(i), i);

            if ((uint8_t)(i - PrevStateChange) < HEARTBEAT_STATE_UNCHANGED_MIN)
            {
                if (HalfPeriodsOK == 0u)
                {
                    DEBUG_LOG("Current level, skip\n");
                    HalfPeriodsOK++;
                    PrevStateChange = i;
                    PrevState = GetSampleAt(i);
                    continue;
                }
                else
                {
                    DEBUG_LOG("ERR too short, i = %u, prevstatechange = %u, diff = %u\n", i, PrevStateChange, i - PrevStateChange);
                    return STATUS_ERROR;
                }
            }
            else if ((uint8_t)(i - PrevStateChange) > HEARTBEAT_STATE_UNCHANGED_MAX)
            {
                DEBUG_LOG("ERR too long, i = %u, prevstatechange = %u, diff = %u\n", i, PrevStateChange, i - PrevStateChange);
                return STATUS_ERROR;
            }
            else
            {
                DEBUG_LOG("OK, i = %u, PrevStateChange = %u, diff = %u, HalfPeriodsOK %u\n", i, PrevStateChange, i - PrevStateChange, HalfPeriodsOK + 1);
                PrevState = GetSampleAt(i);
                PrevStateChange = i;
                HalfPeriodsOK++;
                if (HalfPeriodsOK >= HEARTBEAT_PERIODS_CHECK * 2u)
                {
                    return STATUS_OK;
                }
            }
        }
    }


    return STATUS_ERROR;
}

void heartbeat_init(void)
{
    for (uint8_t i = 0u; i < sizeof(LastStates)/sizeof(LastStates[0u]); i++)
    {
        LastStates[i] = PINSTATE_HIZ;
    }
}

