
Wymagania:

Procesor odbiera sygnał na pinie wejściowym
Moduł heartbeat odczytuje stan pinu z funkcji heartbeat_pin_get – musisz ją zamockować
Poprawny sygnał to fala prostokątna – na przemian 100ms stan niski -> 100ms stan wysoki
Stan pinu odczytujemy co 10 ms
Dopuszczalne wahania 90-110 ms – czyli o 1 odczyt więcej/mniej
Jeżeli sygnał nie spełnia wymagań przez 3 kolejne okresy – sygnalizujemy błąd (wybierz sposób sygnalizacji błędu)
Modyfikacje:

Implementacja bez dodatkowych ograniczeń
Rozszerzenie implementacji na 3 piny
Zmiana tolerancji na 80 – 120 ms
Zmiana długości sygnału na 200 ms
Wykrycie błędu powoduje wejście do funkcji safe state
Wykrycie błędu powoduje zwrócenie odpowiedniej wartości przez funkcję



podsumowanie profilera przed i po optymalizacji z liniowego bufora na ala kołowy.
Testy puszczone 10 tys razy.

  %   cumulative   self              self     total                                                                                                                            
  time   seconds   seconds    calls  us/call  us/call  name      

stare
  93.54      8.40     8.40 11700000     0.72     0.73  heartbeat_thread 
  2.12       8.59     0.19   810000     0.23    10.73  helper_simulate_time                                                                                                      
  1.34       8.71     0.12   180000     0.67     0.67  heartbeat_get_status  
  
  nowe
   23.94      0.37     0.17 11700000     0.01     0.01  heartbeat_thread                                                                               
   28.17      0.20     0.20   810000     0.25     0.46  helper_simulate_time                                                                    
   19.72      0.51     0.14 17629970     0.01     0.01  GetSampleAt                                                                                        
   18.31      0.64     0.13   180000     0.72     0.72  heartbeat_init                                                                                                           
   8.45       0.70     0.06   180000     0.33     1.11  heartbeat_get_status     

Wychodzi na to, że nowa implementacja:
- jest 73 krotnie szybsza dla funkcji heartbeat_thread (która jest najczęściej wołana)
- jest ok 2 krotnie wolniejsza dla heartbeat_get_status, dla funkcji która jest rzadziej wołana (więc dobrze)


************************************
TODO
- uruchomić debugger pod VS code
- zapisać jak uruchomić dokera do wszysktich projektów
- porównać implementacje (porfilerem czy czymś)



