#pragma once

typedef enum{
  PINSTATE_LOW,
  PINSTATE_HIGH,
  PINSTATE_HIZ,
} pinlevel_t;


/**
 * @brief Returns level of pin
 */
pinlevel_t heartbeat_pin_get(void);
