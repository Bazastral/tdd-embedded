#pragma once

#include <stdbool.h>

#define HEARTBEAT_STATE_UNCHANGED_MIN 9u
#define HEARTBEAT_STATE_UNCHANGED_PERFECT 10u
#define HEARTBEAT_STATE_UNCHANGED_MAX 11u
#define HEARTBEAT_PERIOD_SHORTEST (2u*HEARTBEAT_STATE_UNCHANGED_MIN)
#define HEARTBEAT_PERIOD_PERFECT (10u*HEARTBEAT_STATE_UNCHANGED_PERFECT)
#define HEARTBEAT_PERIOD_LONGEST (2u*HEARTBEAT_STATE_UNCHANGED_MAX)
#define HEARTBEAT_PERIODS_CHECK 3u
#define HEARTBEAT_TIMESPAN_MAX (HEARTBEAT_PERIODS_CHECK * HEARTBEAT_PERIOD_LONGEST) + 1
#define HEARTBEAT_TIMESPAN_MIN (HEARTBEAT_PERIODS_CHECK * HEARTBEAT_PERIOD_SHORTEST)


typedef enum {
    STATUS_OK,
    STATUS_ERROR,
    STATUS_UNKNOWN
} heartbeat_status_t;

void heartbeat_init(void);
void heartbeat_thread(void);
heartbeat_status_t heartbeat_get_status(void);
