
#include "hw_api.h"
#include "hw_api_mock.h"


pinlevel_t pinstate = PINSTATE_HIZ;

pinlevel_t heartbeat_pin_get(void)
{
  return pinstate;
}

void mock_heartbeat_pin_set(pinlevel_t new_state)
{
  pinstate = new_state;
}
