
#include "unity_fixture.h"

TEST_GROUP_RUNNER(heartbeat)
{

   /******************************** BASIC CASES ********************************/
   RUN_TEST_CASE(heartbeat, When_Initialized_Then_StateUnknown);
   RUN_TEST_CASE(heartbeat, When_NotEnoughTimePassed_Then_StateStillUnknown);
   RUN_TEST_CASE(heartbeat, When_EnoughTimePassed_Then_StateIsKnown);

   /******************************** POSITIVE CASES ********************************/
   RUN_TEST_CASE(heartbeat, Given_StartingFromLow_When_PerfectPeriods_Then_StateOK);
   RUN_TEST_CASE(heartbeat, Given_StartingFromHigh_When_PerfectPeriods_Then_StateOK);
   RUN_TEST_CASE(heartbeat, When_TooLongAllPeriods_Then_StateError);
   RUN_TEST_CASE(heartbeat, When_PeriodsMinButStillOK_Then_StateOK);
   RUN_TEST_CASE(heartbeat, When_PeriodsMaxButStillOK_Then_StateOK);

   /******************************** NEGATIVE CASES ********************************/
   RUN_TEST_CASE(heartbeat, When_TooShortAllPeriods_Then_StateError);
   RUN_TEST_CASE(heartbeat, When_PeriodsDifferentButStillOk_Then_StateOK);
   RUN_TEST_CASE(heartbeat, When_PeriodsDifferentButStillOk_Then_StateOK);
   RUN_TEST_CASE(heartbeat, When_HighAllTheTime_Then_StateError);
   RUN_TEST_CASE(heartbeat, When_HighAllTheTime_Then_StateError);
   RUN_TEST_CASE(heartbeat, When_LowAllTheTime_Then_StateError);

   /******************************** COMPLEX CASES ********************************/
   RUN_TEST_CASE(heartbeat, When_WasBadButRecovered_Then_StateOK);
   RUN_TEST_CASE(heartbeat, When_WasOKButCurrentLevelIsTooShort_Then_StateOK);
   RUN_TEST_CASE(heartbeat, When_WasOKButPrevLevelWasTooShort_Then_StateError);
   RUN_TEST_CASE(heartbeat, When_WasOKButCurrentLevelIsTooLong_Then_StateError);
}
