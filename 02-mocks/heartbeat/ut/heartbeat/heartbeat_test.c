#include "unity_fixture.h"

#include "heartbeat.h"
#include "hw_api.h"
#include "hw_api_mock.h"

TEST_GROUP(heartbeat);

TEST_SETUP(heartbeat)
{
    /* Init before every test */
}

TEST_TEAR_DOWN(heartbeat)
{
    /* Cleanup after every test */
}

static void helper_simulate_time(uint8_t time, pinlevel_t state)
{
  mock_heartbeat_pin_set(state);

  for (uint8_t i = 0u; i < time; i++)
  {
    heartbeat_thread();
  }
}

static void helper_simulate_period(uint8_t hi, uint8_t low)
{
  helper_simulate_time(hi, PINSTATE_HIGH);
  helper_simulate_time(low, PINSTATE_LOW);
}

/******************************** BASIC CASES ********************************/
TEST(heartbeat, When_Initialized_Then_StateUnknown)
{
  heartbeat_init();
  TEST_ASSERT_EQUAL(STATUS_UNKNOWN, heartbeat_get_status());

}

TEST(heartbeat, When_NotEnoughTimePassed_Then_StateStillUnknown)
{
  heartbeat_init();
  helper_simulate_time(HEARTBEAT_TIMESPAN_MIN-1u, PINSTATE_HIGH);
  TEST_ASSERT_EQUAL(STATUS_UNKNOWN, heartbeat_get_status());
}

TEST(heartbeat, When_EnoughTimePassed_Then_StateIsKnown)
{
  heartbeat_init();
  helper_simulate_time(HEARTBEAT_TIMESPAN_MIN, PINSTATE_HIGH);
  // not unknown
  TEST_ASSERT_NOT_EQUAL(STATUS_UNKNOWN, heartbeat_get_status());
}

/******************************** POSITIVE CASES ********************************/
TEST(heartbeat, Given_StartingFromLow_When_PerfectPeriods_Then_StateOK)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  TEST_ASSERT_EQUAL(STATUS_OK, heartbeat_get_status());
}

TEST(heartbeat, Given_StartingFromHigh_When_PerfectPeriods_Then_StateOK)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  TEST_ASSERT_EQUAL(STATUS_OK, heartbeat_get_status());
}


TEST(heartbeat, When_PeriodsMinButStillOK_Then_StateOK)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN, PINSTATE_LOW);
  TEST_ASSERT_EQUAL(STATUS_OK, heartbeat_get_status());
}

TEST(heartbeat, When_PeriodsDifferentButStillOk_Then_StateOK)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN, PINSTATE_LOW);
  TEST_ASSERT_EQUAL(STATUS_OK, heartbeat_get_status());
}

TEST(heartbeat, When_PeriodsMaxButStillOK_Then_StateOK)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX, PINSTATE_LOW);
  TEST_ASSERT_EQUAL(STATUS_OK, heartbeat_get_status());
}

/******************************** NEGATIVE CASES ********************************/
TEST(heartbeat, When_TooLongAllPeriods_Then_StateError)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX + 1u, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX + 1u, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX + 1u, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX + 1u, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX + 1u, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX + 1u, PINSTATE_LOW);
  TEST_ASSERT_EQUAL(STATUS_ERROR, heartbeat_get_status());
}

TEST(heartbeat, When_TooShortAllPeriods_Then_StateError)
{
  heartbeat_init();

  // first state is longer, as this test should not fail because
  // there was not enought time, but because the states changed to fast
  helper_simulate_time(HEARTBEAT_TIMESPAN_MAX, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN - 1u, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN - 1u, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN - 1u, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN - 1u, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MIN - 1u, PINSTATE_LOW);
  TEST_ASSERT_EQUAL(STATUS_ERROR, heartbeat_get_status());
}

TEST(heartbeat, When_LowAllTheTime_Then_StateError)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_TIMESPAN_MAX, PINSTATE_HIGH);
  TEST_ASSERT_EQUAL(STATUS_ERROR, heartbeat_get_status());
}

TEST(heartbeat, When_HighAllTheTime_Then_StateError)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_TIMESPAN_MAX, PINSTATE_HIGH);
  TEST_ASSERT_EQUAL(STATUS_ERROR, heartbeat_get_status());
}



/******************************** COMPLEX CASES ********************************/
TEST(heartbeat, When_WasBadButRecovered_Then_StateOK)
{
  heartbeat_init();

  // firstly should be bad
  helper_simulate_time(HEARTBEAT_TIMESPAN_MAX, PINSTATE_HIGH);

  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  TEST_ASSERT_EQUAL(STATUS_OK, heartbeat_get_status());
}

// this test is weird, but current state is still active, so if it is too short it does not mean
// that the clock is wrong
TEST(heartbeat, When_WasOKButCurrentLevelIsTooShort_Then_StateOK)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);

  helper_simulate_time(3, PINSTATE_LOW);
  TEST_ASSERT_EQUAL(STATUS_OK, heartbeat_get_status());
}

TEST(heartbeat, When_WasOKButPrevLevelWasTooShort_Then_StateError)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(3, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  TEST_ASSERT_EQUAL(STATUS_ERROR, heartbeat_get_status());
}

TEST(heartbeat, When_WasOKButCurrentLevelIsTooLong_Then_StateError)
{
  heartbeat_init();

  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_HIGH);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_PERFECT, PINSTATE_LOW);
  helper_simulate_time(HEARTBEAT_STATE_UNCHANGED_MAX + 1u, PINSTATE_HIGH);
  TEST_ASSERT_EQUAL(STATUS_ERROR, heartbeat_get_status());
}

