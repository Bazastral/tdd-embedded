
#pragma once 

#include "adc.h"

/**
 * @brief Mock ADC value.
 * 
 * @param[in] value - will be then read from ADC module.
 */
void adc_set_value(adc_value_t value);