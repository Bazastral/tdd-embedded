
#include "unity_fixture.h"

static void run_all_tests(void);

TEST_GROUP_RUNNER(batman)
{
   RUN_TEST_CASE(batman, When_Initialized_Then_NoAlarm);
   RUN_TEST_CASE(batman, When_5readsBelowThreshold_Then_Alarm);
   RUN_TEST_CASE(batman, When_5readsAtThreshold_Then_NoAlarm);
   RUN_TEST_CASE(batman, When_4readsBelowThreshold_Then_NoAlarm);
   RUN_TEST_CASE(batman, Given_AlarmIsSet_When_ReadsAboveThreshold_Then_AlarmStillActive);
   RUN_TEST_CASE(batman, When_4ReadsBelowOneAboveOneBelow_Then_AlarmNotSet);
   RUN_TEST_CASE(batman, When_10Above5Below_Then_AlarmIsSet);
}

int main(int argc, const char **argv)
{
   UnityMain(argc, argv, run_all_tests);

   return 0;
}

static void run_all_tests(void)
{
   RUN_TEST_GROUP(batman);
}

