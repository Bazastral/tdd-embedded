

Battery manager - Batman

This task is trivial. Focus in this task is on writing mocks and
interacting with them.


Zadanie - Alarm o niskim stanie baterii
Poziom baterii jest mierzony przez ADC
Wartość ADC odczytujemy z drivera w kolejnych iteracjach obsługi
Jeżeli w 5 kolejnych pomiarach wartość jest poniżej progu - aktywujemy alarm
Próg alarmu to 2850 w jednostkach ADC
Jeśli alarm jest aktywny - zapalamy diodę led
Po starcie programu dioda jest zgaszona
Jeżeli później napięcie wzrośnie powyżej progu - dalej sygnalizujemy alarm