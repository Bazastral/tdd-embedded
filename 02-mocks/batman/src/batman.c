
#include <stdbool.h>

#include "batman.h"
#include "adc.h"

static uint32_t below_cnt = 0;
static bool alarm = false;

#define BELOW_THRESHOLD_MAX_COUNT 5

void batman_init(void)
{
    adc_init();
    below_cnt = 0u;
    alarm = false;
}

void batman_thread(void)
{
    adc_value_t adc_value = adc_get_value();

    if(adc_value < BATMAN_LOW_BAT_THRESHOLD)
    {
        below_cnt++;
    }
    else
    {
        below_cnt = 0u;
    }

    if(below_cnt >= BELOW_THRESHOLD_MAX_COUNT)
    {
        alarm = true;
        below_cnt = BELOW_THRESHOLD_MAX_COUNT;
    }
}

bool batman_is_alarm(void)
{
    return alarm;
}