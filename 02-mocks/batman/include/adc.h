
#pragma once

#include <stdint.h>


typedef uint16_t adc_value_t;

/**
 * @brief Initialize ADC module
 */
void adc_init(void);

/**
 * @brief Read current ADC value
 */
adc_value_t adc_get_value(void);