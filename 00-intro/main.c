

#include <stdint.h>
#include <stdbool.h>

typedef struct score_frame_{
  uint8_t first;
  uint8_t second;
  uint8_t third;
  uint16_t total;
} score_frame_t;

typedef struct test_{
  score_frame_t game[10];
  uint16_t total_score;
} game_with_total_score_t;

game_with_total_score_t test_games[] =
{
  {
    {
      { 6, 2, 0 },
      { 5, 4, 0 },
      { 5, 5, 0 },
      { 6, 2, 0 },
      {10, 0, 0 },
      { 7, 2, 0 },
      { 6, 4, 0 },
      {10, 0, 0 },
      { 7, 2, 0 },
      {10, 6, 3 }
    }, 136u
  },
  {
    {
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 10, 10 }
    }, 300u
  },
  {
    {
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 }
    }, 0u
  },
  {
    {
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 },
      {0, 0, 0 }
    }, 0u
  },
  {
    {
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {10, 0, 0 },
      {9, 1, 3 }
    }, 272u
  },
}
;

static bool IsStrike(score_frame_t f)
{
  return f.first == 10u;
}

static bool IsSpare(score_frame_t f)
{
  return (f.first + f.second) == 10u;
}

static int calculate_score(score_frame_t s[])
{
  int total_score = 0;

  for(uint8_t i = 0; i < 10u; i++)
  {
    s[i].total = s[i].first + s[i].second;
    if(IsStrike(s[i]))
    {
      if(i < 9u)
      {
        s[i].total += s[i+1].first;
        if(IsStrike(s[i+1]))
        {
          if(i < 8u)
          {
            s[i].total += s[i+2].first;
          }
          else
          {
            s[i].total += s[i+1].second;
          }
        }
        else
        {
          s[i].total += s[i+1].second;
        }
      }
    }
    else if(IsSpare(s[i]))
    {
      if(i < 9u)
      {
        s[i].total += s[i+1].first;
      }
    }
    if(i == 9u)
    {
      s[i].total += s[i].third;
    }
    total_score += s[i].total;
  }
  return total_score;
}



int main(void)
{
  for(uint8_t i = 0; i < sizeof(test_games)/sizeof(test_games[0]); i++)
  {
    int score = calculate_score(test_games[i].game);
    if(score != test_games[i].total_score)
    {
      return i;
    }
  }
  return 0;
}
