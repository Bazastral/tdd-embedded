# tdd-embedded

Materials for Test-Driven-Development in embedded systems course.

Remember to update submodules

## Run project
```
git submodules update --init --recursive
```

### Lesson 0
Aim: implement counting total score in bowling WITHOUT TDD.
Use your old style of implementation.

```
./builder/docker.sh
cd 00-intro
mkdir build && cd build
cmake .. && cmake --build . && ./bowling
# note that program returns 0 if succeed
```

### Lesson 1
Aim: implement counting total score in bowling USING TDD.

```
./builder/docker.sh
cd 01-first-steps/M01-bowling-config/test/bowling/
mkdir build && cd build
cmake .. && cmake --build . && ./bowling_test
```

### Lesson 2
Aim: learn how to implement mocks on your own.
There are two homeworks done in this lesson.

#### Batman
Battery manager. After few reads from ADC below treshold, alarm should be raised.

```
./builder/docker.sh
cd 02-mocks/batman/
mkdir build && cd build
cmake .. && cmake --build . && ./batman
```

#### Heartbeat
Monitor sanity of heartbeat signal. If it is too slow, or to fast, raise alarm.

```
./builder/docker.sh
cd 02-mocks/heartbeat/ut/heartbeat/
mkdir build && cd build
cmake .. && cmake --build . && ./heartbeat_test
```

### Lesson 3 
Aim: learn other UT frameworks than Unity.
Skipped, as I do use Google Test and GMock in work on a daily basis.

### Lesson 4
Aim: run UT on target.
In this lesson UT's are run on target: ATmega32. Results are printed on console wia
UART.
I still did mock ADC readings, as I wanted to make Unit Tests, not integration tests.
There can be added integration, manual test in which real ADC readings from potentiometer
would be used.

1. Wire up ATmega32 (use internall oscillator 8MHz)
2. Connect USBasp programmator
3. Wire UART dongle, connect ATmega's PORTD1 to dongle's RX
4. Setup minicom on host.
    - 9600 8bits, parity non, 1 bit stop
    - Make sure that HW correction / control is turned off.
5. Connect LED between Vcc and ATmega's PORTD7 (add resistor).
6. Run in console:
```
cd 04-embed/homework/
./setup.sh # this will enter special docker image with AVR tools
mkdir build && cd build
cmake .. && cmake --build .
exit
./program.sh
```

### Lesson 5
Aim: review previous homeworks. Done

### Lesson 6
Aim: handle more difficult tasks during TDD.
Implementation of state machine using TDD.

```
./builder/docker.sh
cd 06-harder-issues/state-machine/ut/sm/
mkdir build && cd build
cmake .. && cmake --build . && ./statemachine
```
