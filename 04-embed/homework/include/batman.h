#pragma once

#include <stdbool.h>

#define BATMAN_LOW_BAT_THRESHOLD  2800

/**
 * @brief Initialize ADC module
 */
void batman_init(void);

/**
 * @brief main thread which should be
 * called once in a specific period
 */
void batman_thread(void);

/**
 * @brief Returns if there is low battery alarm
 */
bool batman_is_alarm(void);