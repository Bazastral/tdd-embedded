
#ifndef __UART_H
#define __UART_H



/**
 * @brief Initialize USART module
 */
void USARTInit(void);

/**
 * @brief Send byte via USART
 */
int USARTSendByte(int u8Data);

/**
 * @brief Send string (NULL-terminated array of bytes) via USART
 */
void USARTSendString(char str[]);

/**
 * @brief Get one byte from USART
 */
uint8_t USARTReceiveByte(void);

#endif // __UART_H