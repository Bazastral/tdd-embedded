
#include <avr/io.h>
#include "uart.h"

#define USART_BAUDRATE 9600
#define UBRR_VALUE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)
#define MAX_STRING_LEN 200

void USARTInit(void)
{
    // Set baud rate
    // URSEL must be 0
    UBRRH = (uint8_t)(UBRR_VALUE >> 8) & 0x7F;
    // UBRRH = 0u;
    UBRRL = (uint8_t)UBRR_VALUE;
    // Set frame format to 8 data bits, no parity, 1 stop bit
    UCSRC = (1 << URSEL) | (0 << USBS) | (1 << UCSZ1) | (1 << UCSZ0);
    // enable transmission and reception
    UCSRB |= (1 << RXEN) | (1 << TXEN);

    OSCCAL = 185;
}

int USARTSendByte(int u8Data)
{
    if (u8Data < __UINT8_MAX__)
    {
        // wait while previous byte is completed
        while (!(UCSRA & (1 << UDRE)))
        {
        };
        // Transmit data
        UDR = u8Data;
    }
    return 0u;
}

uint8_t USARTReceiveByte()
{
    // Wait for byte to be received
    while (!(UCSRA & (1 << RXC)))
    {
    };
    // Return received data
    return UDR;
}

void USARTSendString(char str[])
{
    for (uint8_t i = 0; i < MAX_STRING_LEN; i++)
    {
        if (str[i] == 0u)
        {
            break;
        }
        USARTSendByte(str[i]);
    }
}