
#include <util/delay.h>
#include <avr/io.h>

#include "unity_fixture.h"
#include "uart.h"

static void run_all_tests(void);

int main(int argc, const char **argv)
{
   int failures;
   USARTInit();
   // turn on led before tests
   DDRD |= (1 << DDD7);
   PORTD &= ~(1 << DDD7);

   USARTSendString("\n\n\nStart tests!\n");
   failures = UnityMain(argc, argv, run_all_tests);

   // turn off led after tests
   PORTD |= (1 << DDD7);
   while(1)
   {
      // signalize that tests failed
      if(failures > 0)
      {
         _delay_ms(500);
         PORTD ^= (1 << DDD7);
      }
   }

   return 0;
}

static void run_all_tests(void)
{
   RUN_TEST_GROUP(batman);
}

TEST_GROUP_RUNNER(batman)
{
   RUN_TEST_CASE(batman, When_Initialized_Then_NoAlarm);
   RUN_TEST_CASE(batman, When_5readsBelowThreshold_Then_Alarm);
   RUN_TEST_CASE(batman, When_5readsAtThreshold_Then_NoAlarm);
   RUN_TEST_CASE(batman, When_4readsBelowThreshold_Then_NoAlarm);
   RUN_TEST_CASE(batman, Given_AlarmIsSet_When_ReadsAboveThreshold_Then_AlarmStillActive);
   RUN_TEST_CASE(batman, When_4ReadsBelowOneAboveOneBelow_Then_AlarmNotSet);
   RUN_TEST_CASE(batman, When_10Above5Below_Then_AlarmIsSet);
}
