
#include "unity_fixture.h"

#include "batman.h"
#include "adc_mock.h"


TEST_GROUP(batman);

TEST_SETUP(batman)
{
    /* Init before every test */
    batman_init();
}

TEST_TEAR_DOWN(batman)
{
    /* Cleanup after every test */
}

static void below_threshold(uint8_t times)
{
    adc_set_value(BATMAN_LOW_BAT_THRESHOLD - 1);
    for(uint8_t i = 0u; i < times; i++)
    {
        batman_thread();
    }
}

static void above_threshold(uint8_t times)
{
    adc_set_value(BATMAN_LOW_BAT_THRESHOLD);
    for(uint8_t i = 0u; i < times; i++)
    {
        batman_thread();
    }
}

/******************************** BASIC CASES ********************************/
TEST(batman, When_Initialized_Then_NoAlarm)
{
    TEST_ASSERT_EQUAL(false, batman_is_alarm());
}

TEST(batman, When_5readsBelowThreshold_Then_Alarm)
{
    below_threshold(5);
    TEST_ASSERT_EQUAL(true, batman_is_alarm());
}

TEST(batman, When_5readsAtThreshold_Then_NoAlarm)
{
    above_threshold(5);
    TEST_ASSERT_EQUAL(false, batman_is_alarm());
}

TEST(batman, When_4readsBelowThreshold_Then_NoAlarm)
{
    below_threshold(4);
    TEST_ASSERT_EQUAL(false, batman_is_alarm());
}

TEST(batman, Given_AlarmIsSet_When_ReadsAboveThreshold_Then_AlarmStillActive)
{
    below_threshold(5);
    above_threshold(5);
    TEST_ASSERT_EQUAL(true, batman_is_alarm());
}

TEST(batman, When_4ReadsBelowOneAboveOneBelow_Then_AlarmNotSet)
{
    below_threshold(4);
    above_threshold(1);
    below_threshold(1);

    TEST_ASSERT_EQUAL(false, batman_is_alarm());
}

TEST(batman, When_10Above5Below_Then_AlarmIsSet)
{
    above_threshold(10);
    below_threshold(5);

    TEST_ASSERT_EQUAL(true, batman_is_alarm());
}