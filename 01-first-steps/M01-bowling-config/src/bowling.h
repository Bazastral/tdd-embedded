/**
 * Bowling kata rules: https://kata-log.rocks/bowling-game-kata
 *
 */ 

/**
 * @brief Initialize module
 */
void init(void);

/**
 * @brief One roll.
 * @param[in] val - how many pins have fallen
 */
void roll(int val);

/**
 * @brief Returns score of a game
 * Tested only on end of whole game.
 * Calling function before end of game
 * results in Undefined Behavior.
 */
int score(void);


