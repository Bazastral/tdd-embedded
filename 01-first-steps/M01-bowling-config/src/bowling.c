#include "bowling.h"

#include "stdbool.h"

enum strike_state
{
    NO_STRIKE,
    SINGLE_STRIKE_ONE_ROLL,
    SINGLE_STRIKE_TWO_ROLLS,
    DOUBLE_STRIKE,
};

static int current_score = 0;
static bool is_spare = false;
static enum strike_state strike_state = NO_STRIKE;
static int last_val = 0;
static int roll_cnt = 0;

static void check_if_spare(int val);
static void add_bonus_for_spare(int val);
static void add_bonus_for_strike(int val);
static bool is_first_roll_in_turn(void);
static bool is_second_roll_in_turn(void);
static void update_lastval_for_next_roll(int val);
static bool is_bonus_turn(void);

void init(void)
{
    current_score = 0;
    is_spare = false;
    strike_state = NO_STRIKE;
    last_val = 0;
    roll_cnt = 0;
}



void roll(int val)
{
    roll_cnt++;

    if (is_bonus_turn())
    {
        add_bonus_for_spare(val);
        add_bonus_for_strike(val);
    }
    else
    {
        current_score += val;

        add_bonus_for_spare(val);
        check_if_spare(val);

        add_bonus_for_strike(val);

        update_lastval_for_next_roll(val);
    }


}

int score(void)
{
    return current_score;
}

static bool is_first_roll_in_turn(void)
{
    return roll_cnt % 2 != 0;
}

static bool is_second_roll_in_turn(void)
{
    return roll_cnt % 2 == 0;
}

static void check_if_spare(int val)
{
    if (is_second_roll_in_turn() && (last_val + val == 10))
    {
        is_spare = true;
    }
}

static void add_bonus_for_spare(int val)
{
    if (is_spare)
    {
        current_score += val;
        is_spare = false;
    }
}

static void add_bonus_for_strike(int val)
{
    if (strike_state == SINGLE_STRIKE_ONE_ROLL)
    {
        current_score += val;

        strike_state = NO_STRIKE;
    }
    else if (strike_state == SINGLE_STRIKE_TWO_ROLLS)
    {
        current_score += val;

        if (val == 10 && is_first_roll_in_turn())
        {
            strike_state = DOUBLE_STRIKE;
            roll_cnt++;
        }
        else
        {
            strike_state = SINGLE_STRIKE_ONE_ROLL;
        }
    }
    else if (strike_state == DOUBLE_STRIKE)
    {
        current_score += 2 * val;

        if (val == 10 && is_first_roll_in_turn() && !is_bonus_turn())
        {
            strike_state = DOUBLE_STRIKE;
            roll_cnt++;
        }
        else
        {
            strike_state = SINGLE_STRIKE_ONE_ROLL;
        }
    }
    else
    {
        if (val == 10 && is_first_roll_in_turn())
        {
            strike_state = SINGLE_STRIKE_TWO_ROLLS;
            roll_cnt++;
        }
    }
}

static void update_lastval_for_next_roll(int val)
{
    if (is_first_roll_in_turn())
    {
        last_val = val;
    }
    else
    {
        last_val = 0;
    }
}

static bool is_bonus_turn(void)
{
    return roll_cnt > 10 * 2;
}
