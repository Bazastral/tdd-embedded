#include "unity/fixture/unity_fixture.h"

#include "bowling.h"
#include "assert.h"

TEST_GROUP(bowling);

/* This function don't calculate bonus points */
int strike(void)
{
    roll(10);

    return 10;
}

int turn(int first, int second)
{
    roll(first);
    roll(second);

    return first + second;
}

/* This function don't calculate bonus points */
int spare(int first, int second)
{
    assert (first != 10);
    assert (first + second == 10);

    return turn(first, second);
}

int strike_bonus(int first, int second)
{
    return first + second;
}

int spare_bonus(int val)
{
    return val;
}

TEST_SETUP(bowling)
{
    /* Init before every test */
    init();
}

TEST_TEAR_DOWN(bowling)
{
    /* Cleanup after every test */
}

TEST(bowling, WhenRolled1ScoreIs1)
{
    roll(1);

    TEST_ASSERT_EQUAL(1, score());
}

TEST(bowling, WhenRolled1and2ScoreIs3)
{
    roll(1);
    roll(2);
    
    TEST_ASSERT_EQUAL(3, score());
}

TEST(bowling, WhenRolled10inTwoRollsScoreFromNextShotIsAdded)
{
    int expected = 0;

    expected += spare(3, 7);

    roll(5);
    expected += 5;

    expected += spare_bonus(5);
    
    TEST_ASSERT_EQUAL(expected, score());
}

TEST(bowling, WhenSpareInSecondTurnThenScoreFromThirdAdded)
{
    int expected = 0;

    expected += turn(5, 1);
    expected += spare(3, 7);

    roll(5);
    expected += 5;

    expected += spare_bonus(5);

    TEST_ASSERT_EQUAL(expected, score());
}

TEST(bowling, WhenSecondShotFromFirstRundAndFirstFromSecondEquals10ThenSpareBonusIsNotAdded)
{
    int expected = 0;

    expected += turn(1, 5);
    expected += turn(5, 1);

    TEST_ASSERT_EQUAL(expected, score());
}

TEST(bowling, SpareInTwoTurnsInTheRow)
{
    int expected = 0;

    expected += spare(5, 5);
    expected += spare(7, 3);

    expected += spare_bonus(7);

    roll(5);
    expected += 5;

    expected += spare_bonus(5);

    TEST_ASSERT_EQUAL(expected, score());
}

TEST(bowling, WhenRolledStrikeScoreFromNextTwoRollsAreAdded)
{
    int expected = 0;

    expected += strike();
    expected += turn(3, 3);

    expected += strike_bonus(3, 3);

    TEST_ASSERT_EQUAL(expected, score());
}

TEST(bowling, TwoStrikesInARow)
{
    int expected = 0;

    expected += strike();
    expected += strike();
    expected += turn(3, 3);

    expected += strike_bonus(10 ,3);
    expected += strike_bonus(3 ,3);

    TEST_ASSERT_EQUAL(expected, score());
}

TEST(bowling, StrikeThenSpare)
{
    int expected = 0;

    expected += strike();
    expected += spare(3, 7);

    expected += strike_bonus(7 ,3);

    expected += turn(5, 3);

    expected += spare_bonus(5);

    TEST_ASSERT_EQUAL(expected, score());
}

TEST(bowling, When10inSecondRollInTurn)
{
    int expected = 0;

    expected += spare(0, 10);
    expected += turn(3, 3);

    expected += spare_bonus(3);

    TEST_ASSERT_EQUAL(expected, score());
}

TEST(bowling, GivenLastTurnWhenSpareAdditionalRoll)
{
    int expected = 0;
    
    for (int i = 0; i < 9; i++)
    {
        expected += turn(0, 0);
    }
    
    expected += spare(3, 7);

    /* This roll is not counted by itself - its only needed for spare bonus */
    roll(3);

    expected += spare_bonus(3);

    TEST_ASSERT_EQUAL(expected, score());
}

TEST(bowling, GivenLastTurnWhenStrikeAdditionalTwoRolls)
{
    int expected = 0;
    
    for (int i = 0; i < 9; i++)
    {
        expected += turn(0, 0);
    }
    
    expected += strike();

    /* This roll is not counted by itself - its only needed for spare bonus */
    roll(3);
    roll(5);

    expected += strike_bonus(3, 5);

    TEST_ASSERT_EQUAL(expected, score());
}

TEST(bowling, MaxPoints)
{
    for (int i = 0; i < 12; i++)
    {
        strike();
    }

    TEST_ASSERT_EQUAL(300, score());
}