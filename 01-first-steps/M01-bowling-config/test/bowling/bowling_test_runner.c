#include "unity/fixture/unity_fixture.h"

TEST_GROUP_RUNNER(bowling)
{
   /* Test cases to run */
   RUN_TEST_CASE(bowling, WhenRolled1ScoreIs1);
   RUN_TEST_CASE(bowling, WhenRolled1and2ScoreIs3);
   RUN_TEST_CASE(bowling, WhenRolled10inTwoRollsScoreFromNextShotIsAdded);
   RUN_TEST_CASE(bowling, WhenSpareInSecondTurnThenScoreFromThirdAdded);
   RUN_TEST_CASE(bowling, WhenSecondShotFromFirstRundAndFirstFromSecondEquals10ThenSpareBonusIsNotAdded);
   RUN_TEST_CASE(bowling, SpareInTwoTurnsInTheRow);
   RUN_TEST_CASE(bowling, WhenRolledStrikeScoreFromNextTwoRollsAreAdded);
   RUN_TEST_CASE(bowling, TwoStrikesInARow);
   RUN_TEST_CASE(bowling, StrikeThenSpare);
   RUN_TEST_CASE(bowling, When10inSecondRollInTurn);
   RUN_TEST_CASE(bowling, GivenLastTurnWhenSpareAdditionalRoll);
   RUN_TEST_CASE(bowling, GivenLastTurnWhenStrikeAdditionalTwoRolls);
   RUN_TEST_CASE(bowling, MaxPoints);
}
