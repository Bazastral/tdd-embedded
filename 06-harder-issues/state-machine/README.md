

Task:

Detect special signal on pin.
Special signal must:
- last max 3s
- have 3x high level, each for at least 200ms
- begin and end with low level

Once special signal is detected, module calls callback and does not have to detect anything else.

Assumption:
Function which checks signal detection will be called once every 100ms