#pragma once

typedef enum{
  PINLEVEL_LOW,
  PINLEVEL_HIGH,
} pinlevel_t;


/**
 * @brief Returns level of pin
 */
pinlevel_t sm_pin_get(void);
