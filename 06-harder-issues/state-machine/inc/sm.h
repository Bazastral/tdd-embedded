#pragma once

#include <stdbool.h>

#define SM_WHOLE_SIGNAL_MAX_TIME_S 3u
#define SM_ONE_HIGH_LEVEL_MIN_TIME_MS 200u

typedef void sm_detection_callback(void);

/**
 * @brief Initialization of the module
 * 
 * @param[in] callback  function which will be called when specific signal will be detected
 */
void sm_init(sm_detection_callback callback);

/**
 * @brief Periodic task which checks state of a pin
 * Must be called once every 100ms
 */
void sm_thread(void);

