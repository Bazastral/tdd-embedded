
#include <stdint.h>
#include <stddef.h>

#include "hw_api.h"
#include "sm.h"

#define SECONDS_TO_100MS(x) (10 * x)
#define MS_TO_100MS(x) (x / 100)
#define HIGH_PEAKS_TO_DETECT 3

/**
 * Timer is incremented every 100ms
 */
static uint8_t timer = 0u;
static sm_detection_callback *detection_callback = NULL;
/** timestamps of edges */
static uint8_t edge_tstamps[(HIGH_PEAKS_TO_DETECT * 2u) - 1u] = {0u};
static uint8_t edges_detected = 0u;
static bool first_low_detected = false;
static pinlevel_t pinlvl_prev = PINLEVEL_LOW;
static bool signal_detected = false;

void sm_init(sm_detection_callback callback)
{
    printf("\n\n\nINIT\n");
    detection_callback = callback;
    timer = 0u;
    edges_detected = 0u;
    first_low_detected = false;
    pinlvl_prev = PINLEVEL_LOW;
    signal_detected = false;
    memset(edge_tstamps, 0u, sizeof(edge_tstamps));
}

void sm_thread(void)
{
    if (signal_detected == false)
    {

        pinlevel_t pinlvl_now = sm_pin_get();

        printf("--- pinlvl_now = %s, time %d, changes %d, ",
               (pinlvl_now == PINLEVEL_HIGH) ? "HI" : "LO",
               timer, edges_detected);
        for (uint8_t i = 0u; i < edges_detected; i++)
        {
            printf("[%d] = %d, ", i, edge_tstamps[i]);
        }
        printf("\n");

        if (first_low_detected)
        {
            // timeout for whole signal
            if (timer - edge_tstamps[0] >= SECONDS_TO_100MS(3))
            {
                printf("3s TIMEOUT\n");
                if (edges_detected >= 2u)
                {
                    edges_detected -= 2u;
                    // reject the oldest peak
                    for (uint16_t i = 0u; i < edges_detected; i++)
                    {
                        edge_tstamps[i] = edge_tstamps[i + 2u];
                    }
                }

                printf(">>> pinlvl_now = %s, time %d, changes %d, ",
                       (pinlvl_now == PINLEVEL_HIGH) ? "HI" : "LO",
                       timer, edges_detected);
                for (uint8_t i = 0u; i < edges_detected; i++)
                {
                    printf("[%d] = %d, ", i, edge_tstamps[i]);
                }
                printf("\n");
            }
        }

        if (pinlvl_prev == pinlvl_now)
        {
            printf("no change\n");
            // no change

            if (first_low_detected == false)
            {
                first_low_detected = true;
            }
        }
        else if (first_low_detected == true)
        {
            printf("change from %s to %s\n",
                   (pinlvl_prev == PINLEVEL_HIGH) ? "HI" : "LO",
                   (pinlvl_now == PINLEVEL_HIGH) ? "HI" : "LO");
            if (((timer - edge_tstamps[edges_detected - 1u]) < MS_TO_100MS(SM_ONE_HIGH_LEVEL_MIN_TIME_MS)) &&
                (pinlvl_prev == PINLEVEL_HIGH))
            {
                if (edges_detected >= 1u)
                {
                    printf("TOO SHORT HIGH STATE %d,  %d\n", edge_tstamps[edges_detected - 1u], timer);
                    // too short
                    edges_detected--;
                }
                else
                {
                    // should never reach this, as pintlevel high means that at least one change occured
                }
            }
            else
            {
                printf("ok\n");
                // change in state
                edge_tstamps[edges_detected] = timer;
                edges_detected++;
                if (edges_detected >= HIGH_PEAKS_TO_DETECT * 2u)
                {
                    printf("CALLBACK!!!\n");
                    signal_detected = true;
                    if (detection_callback)
                    {
                        detection_callback();
                    }
                }
            }
        }
        else
        {
            printf("There was no low state\n");
        }
        pinlvl_prev = pinlvl_now;
        // update local timer
        timer++;
    }
}
