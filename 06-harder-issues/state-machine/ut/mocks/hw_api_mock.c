
#include "hw_api.h"
#include "hw_api_mock.h"


pinlevel_t pinlevel = PINLEVEL_LOW;

pinlevel_t sm_pin_get(void)
{
  return pinlevel;
}

void mock_sm_pin_set(pinlevel_t new_state)
{
  pinlevel = new_state;
}
