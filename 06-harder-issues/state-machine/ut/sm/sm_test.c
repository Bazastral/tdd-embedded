#include "unity_fixture.h"

#include "sm.h"
#include "hw_api.h"
#include "hw_api_mock.h"


#define LOW_STATE_MS 100u

static uint8_t detected = 0u;

static void detected_callback(void)
{
  detected++;
}

static uint16_t set_pinlevel_for_ms(pinlevel_t lvl, uint16_t milliseconds)
{
  mock_sm_pin_set(lvl);
  for(uint16_t i = 0; i < (milliseconds+99) / 100; i++)
  {
    sm_thread();
  }
  return milliseconds;
}

TEST_GROUP(sm);

TEST_SETUP(sm)
{
  detected = 0;
    /* Init before every test */
}

TEST_TEAR_DOWN(sm)
{
    /* Cleanup after every test */
}


TEST(sm, When_Initialized_Then_NotDetected)
{
  sm_init(detected_callback);
  TEST_ASSERT_EQUAL(0, detected);
}

TEST(sm, When_ThreeHighStatesFor200ms_Then_Detected)
{
  sm_init(detected_callback);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);

  TEST_ASSERT_EQUAL(1, detected);
}

TEST(sm, When_ThreeHighStatesButOneTooShort_Then_NotDetected)
{
  sm_init(detected_callback);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  //too short
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS / 2);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  TEST_ASSERT_EQUAL(0, detected);
}

/**
 * Whole signal should take less than 3s
 */
TEST(sm, When_ThreeHighStatesButWholeLastedTooLong_Then_NotDetected)
{
  sm_init(detected_callback);
  uint16_t total_ms = 0;

  total_ms += set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  total_ms += set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  total_ms += set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  total_ms += set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  total_ms += set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);

  // wait until more than 3s passed
  while(total_ms < SM_WHOLE_SIGNAL_MAX_TIME_S*1000u + 1u)
  {
    total_ms += set_pinlevel_for_ms(PINLEVEL_HIGH, 100);
  }

  total_ms += set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);

  TEST_ASSERT_EQUAL(0, detected);
}

TEST(sm, When_ThreeHighStatesExactly3s_Then_Detected)
{
  sm_init(detected_callback);
  uint16_t total_ms = 0;

  total_ms += set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  total_ms += set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  total_ms += set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  total_ms += set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  total_ms += set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  // wait until 3s passed (corner case)
  while(total_ms < SM_WHOLE_SIGNAL_MAX_TIME_S*1000u)
  {
    total_ms += set_pinlevel_for_ms(PINLEVEL_HIGH, LOW_STATE_MS);
  }

  total_ms += set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);

  TEST_ASSERT_EQUAL(1, detected);
}

/** 
 * Must start from low level
 */
TEST(sm, When_ThreeHighStatesStartingFromHigh_Then_NotDetected)
{
  sm_init(detected_callback);

  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);

  TEST_ASSERT_EQUAL(0, detected);
}

/** 
 * Must start from low level.
 * Started from high, but followed by 3 valid high levels
 */
TEST(sm, When_FourHighStatesStartingFromHigh_Then_Detected)
{
  sm_init(detected_callback);

  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);

  TEST_ASSERT_EQUAL(1, detected);
}

/** 
 * First high level is too long, but next ones are good
 */
TEST(sm, When_FourHighStatesAndFirstIsTooLong_Then_Detected)
{
  sm_init(detected_callback);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  // too long
  set_pinlevel_for_ms(PINLEVEL_HIGH, 5000);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);

  TEST_ASSERT_EQUAL(1, detected);
}

/** 
 * Long break after first high level.
 * Followed by proper 3 rising and falling edges
 */
TEST(sm, When_LongLowLevelAfterFirstFollowedBy3properHigh_Then_Detected)
{
  sm_init(detected_callback);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  // long low level
  set_pinlevel_for_ms(PINLEVEL_LOW, 2700);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);

  TEST_ASSERT_EQUAL(1, detected);
}

/** 
 * Long break after two high levels
 * Followed by proper 3 rising and falling edges
 */
TEST(sm, When_TwoProperHighAndLongLowAnd3properHigh_Then_Detected)
{
  sm_init(detected_callback);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  // long low level
  set_pinlevel_for_ms(PINLEVEL_LOW, 2800);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);

  TEST_ASSERT_EQUAL(1, detected);
}

/** 
 * More complicated test
 */
TEST(sm, When_3TooShort2OkAnd3TooShort_Then_NotDetected)
{
  sm_init(detected_callback);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, 100);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, 100);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, 100);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, SM_ONE_HIGH_LEVEL_MIN_TIME_MS);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, 100);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, 100);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);
  set_pinlevel_for_ms(PINLEVEL_HIGH, 100);

  set_pinlevel_for_ms(PINLEVEL_LOW, LOW_STATE_MS);

  TEST_ASSERT_EQUAL(0, detected);
}

