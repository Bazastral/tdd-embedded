
cmake_minimum_required(VERSION 3.10)

project(statemachine C)

add_subdirectory(../../../../unity unity)

set(CMAKE_C_FLAGS  "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wpedantic")

set(INCLUDE_DIRS
  ../../inc
  )

set(TEST_INCLUDE_DIRS
  ../../../../unity/core
  ../../../../unity/fixture
  ..
  .
  ../mocks/
  )

set(SRCS
  ../../src/sm.c
  )

set(TEST_SRCS
  sm_test_main.c
  sm_test.c
  )

set(MOCKS
  ../mocks/hw_api_mock.c
  )

include_directories(${INCLUDE_DIRS} ${TEST_INCLUDE_DIRS})

link_libraries(unity)
add_executable(${PROJECT_NAME} ${SRCS} ${TEST_SRCS} ${MOCKS})
set_property(TARGET ${PROJECT_NAME} PROPERTY C_STANDARD 11)

target_compile_definitions(${PROJECT_NAME} PRIVATE
    $<$<CONFIG:Debug>:
        DEBUG=1
    >
)
