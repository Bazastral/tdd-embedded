#include "unity_fixture.h"

static void run_all_tests(void);

int main(int argc, const char **argv)
{
   UnityMain(argc, argv, run_all_tests);

   return 0;
}

static void run_all_tests(void)
{
   RUN_TEST_GROUP(sm);
}


TEST_GROUP_RUNNER(sm)
{
   RUN_TEST_CASE(sm, When_Initialized_Then_NotDetected);
   RUN_TEST_CASE(sm, When_ThreeHighStatesFor200ms_Then_Detected);
   RUN_TEST_CASE(sm, When_ThreeHighStatesButOneTooShort_Then_NotDetected);
   RUN_TEST_CASE(sm, When_ThreeHighStatesButWholeLastedTooLong_Then_NotDetected);
   RUN_TEST_CASE(sm, When_ThreeHighStatesExactly3s_Then_Detected);
   RUN_TEST_CASE(sm, When_ThreeHighStatesStartingFromHigh_Then_NotDetected);
   RUN_TEST_CASE(sm, When_FourHighStatesStartingFromHigh_Then_Detected);
   RUN_TEST_CASE(sm, When_FourHighStatesAndFirstIsTooLong_Then_Detected);
   RUN_TEST_CASE(sm, When_LongLowLevelAfterFirstFollowedBy3properHigh_Then_Detected);
   RUN_TEST_CASE(sm, When_TwoProperHighAndLongLowAnd3properHigh_Then_Detected);
   RUN_TEST_CASE(sm, When_3TooShort2OkAnd3TooShort_Then_NotDetected);
}